﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    /// <summary>
    /// Моделька для создания работника
    /// </summary>
    public class CreateEmployeeRequest
    {
        public string FirstName { get; set; }
        
        public string LastName { get; set; }

        public string Email { get; set; }
    }

    public class CreateEmployeeRequest_MapProfile : Profile
    {
        public CreateEmployeeRequest_MapProfile()
        {
            CreateMap<CreateEmployeeRequest, Employee>();
        }
    }
}